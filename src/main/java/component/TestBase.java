package component;

import config.Configuration;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import pages.Lamdatesthomepage;

import java.io.File;
import java.lang.reflect.Method;
import java.time.Duration;
import java.util.List;

public class TestBase  {

    WebDriver driver = null;
    public static final Logger log = Logger.getLogger(TestBase.class.getName());

    protected String specPath = System.getProperty("user.dir") + "/src/test/java/resources/specFiles/";

    static {

        log.info("Loding the configuration...");
        Configuration.loadConfig();
        PropertyConfigurator.configure(new File("src/main/java/config/log4j.properties").getAbsolutePath());
    }

    public WebDriver getDriver () {
        //WebDriver driver = this.driver.get();
        if (driver == null) {
            //throw new RuntimeException("The driver is not instantiated yet");
            driver = createDriver();
        }
        return driver;
    }

    public void start () {

        load(Configuration.getUrl());
        Util.waitForSec(3);
        //click(By.cssSelector("#lnklogin"));
        Util.waitForSec(5);
    }


    public void login () {
        enterValue(By.xpath("//input[@placeholder='Email']"), Configuration.getUser());
        enterValue(By.xpath("//input[@placeholder='Password']"), "");
        click(By.xpath("//*[@id=\"theSubmit\"]"));
        Util.waitForSec(2);


    }

    public void click (By by) {
        waitForElement(by);
        javaScriptScrollHighlight(by);
        getDriver().findElement(by).click();
        Util.waitForSec(1);

    }
    public void javaScriptClick(By by)
    {
        waitForElement(by);
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        WebElement wb= getDriver().findElement(by);
        javaScriptScrollHighlight(by);
        js.executeScript("arguments[0].click();arguments[1].click();", wb , wb);
        Util.waitForSec(1);

    }

    public void mouseRightClick(By by)
    {

        Actions actions=new Actions(getDriver());
        WebElement wb=getDriver().findElement(by);
        javaScriptScrollHighlight(by);
        actions.contextClick(wb).build().perform();

    }

    public void mouseDoubleClick(By by)
    {

        Actions actions=new Actions(getDriver());
        WebElement wb=getDriver().findElement(by);
        javaScriptScrollHighlight(by);
        actions.doubleClick(wb).perform();

    }


    public void enterValue (By by, String value) {
        waitForElement(by);
        // getDriver().findElement( by ).clear();
        javaScriptScrollHighlight(by);
        getDriver().findElement(by).sendKeys(value);

    }
    public void javaScriptTypeByID(String xPath,String textValue)
    {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("document.getElementById('"+xPath+"').setAttribute('value', '"+textValue+"')");
        javaScriptClick(Lamdatesthomepage.lamdatestbutton1);
    }








    public void waitForElement (By element) {
        try {
            WebDriverWait wait = new WebDriverWait(getDriver(), Duration.ofMinutes(1));
            wait.until(ExpectedConditions.visibilityOf(getDriver().findElement(element)));
        } catch (Exception e) {
            log.info(e.getMessage());
        }
    }


    public WebDriver createDriver () {
        WebDriver driver = null;
        String driverPath = System.getProperty("user.dir") + "/src/main/java/config/";
        switch (Configuration.getBrowser()) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                ChromeOptions options = new ChromeOptions();
               // options.addArguments("disable-infobars");
               // if (Configuration.getHeadless()) options.addArguments("headless");
                driver = new ChromeDriver();
                break;
          /*  case "firefox":
                System.setProperty(Configuration.fireFoxKey, driverPath + "geckodriver.exe");
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                if (Configuration.getHeadless()) firefoxOptions.addArguments("-headless");
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case "ie":
                System.setProperty(Configuration.ieKey, driverPath + "IEDriverServer.exe");
                driver = new InternetExplorerDriver();
                break;
            case "Edge":
                System.setProperty(Configuration.edgeKey, driverPath + "MicrosoftWebDriver.exe");
                driver = new EdgeDriver();
                break;*/

            default:
                Assert.assertTrue(false, "Browser is not mentioned correctly : " + Configuration.getBrowser());

        }
/*        if (args.length > 0) {
            if (args[0] != null && args[0] instanceof TestDevice) {
                TestDevice device = (TestDevice) args[0];
                if (device.getScreenSize() != null) {
                    driver.manage().window().setSize( device.getScreenSize() );

                }
            }
        }*/
        return driver;
    }

    public void load (String url) {
        getDriver().get(url);
    }

/*    public void checkPageLayout(String specFile, List<String> tags) {
        try {
            checkLayout( specFile, tags );

        } catch (Exception e) {
            log.error(e);
            throw new RuntimeException( e );
        }

    }*/


/*
    @DataProvider(name = "devices")
    public Object[][] devices() {
        return new Object[][]{
                // {new TestDevice("mobile", new Dimension(450, 800), asList("mobile"))},
                {new TestDevice( "desktop", new Dimension( 1366, 768 ), asList( "desktop" ) )},
                {new TestDevice( "tablet", new Dimension( 768, 1024 ), asList( "tablet" ) )}

        };
    }
*/




    public void javaScriptScrollDown () {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
       // jse.executeScript("window.scrollBy(0,document.body.scrollHeight);", "");
       jse.executeScript("window.scrollBy(0,600);", "");
        Util.waitForSec(2);
    }

    public void javaScriptScrollUp (int i) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
       // jse.executeScript("window.scrollBy(0,(i * -1));", "");
        jse.executeScript("window.scrollBy(0,-600);", "");
    }

    public void javaScriptScrollHighlight (By by) {
        JavascriptExecutor jse = (JavascriptExecutor) getDriver();
        // jse.executeScript("window.scrollBy(0,document.body.scrollHeight);", "");
        jse.executeScript("window.scrollBy(0,-600);", "");
        WebElement wb=getDriver().findElement(by);
        jse.executeScript("arguments[0].setAttribute('style', 'background: Green; border: 2px solid red;');", wb);
    }
    @BeforeMethod
    public void updateMethodName (Method method, Object[] testData, ITestContext ctx) {
        ThreadLocal<String> testName = new ThreadLocal<>();
        if (testData.length > 0) {
            testName.set(method.getName() + "_" + testData[0]);
            ctx.setAttribute("testName", testName.get());
        } else ctx.setAttribute("testName", method.getName());
    }

    public String getText(By by) {
        return getDriver().findElement(by).getText();
    }

    public void takeSnapShot(String FilePath) throws Exception {

        //Convert web driver object to TakeScreenshot
        /*
        TakesScreenshot scrShot =((TakesScreenshot)getDriver());
        //Call getScreenshotAs method to create image file
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        //Move image file to new destination
        File DestFile=new File(System.getProperty("user.dir")+"//target//Screenshots//"+FilePath+".png");
        //Copy file at destination
        FileUtils.copyFile(SrcFile, DestFile);

/*
        File file=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(file,new File(""+FilePath+".png")); */

        File file = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);

        FileUtils.copyFile(file, new File("C://Users//akshata.dongare//MavenProject//target//Screenshot//" + FilePath + ".png"));

    }




}
