package tests;

import component.TestBase;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.GoogleHomePage;

public class Regression_Test_2 extends TestBase {

    @Test
    public void testRegression_Test_21(){

        log.info("Opening the Url...");
        start();
        enterValue(GoogleHomePage.searchBox,"Mayur"+ Keys.ENTER);
        //click(GoogleHomePage.googleSearchButton);
        Assert.assertTrue(getDriver().getTitle().contains("Mayur"));
    }

    @Test
    public void testRegression_Test_22(){

        log.info("Opening the Url...");
        start();
        enterValue(GoogleHomePage.searchBox,"Mayur"+ Keys.ENTER);
        //click(GoogleHomePage.googleSearchButton);
        Assert.assertTrue(getDriver().getTitle().contains("Mayur"));
    }
}
