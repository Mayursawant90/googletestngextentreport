package tests;

import component.TestBase;
import component.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import pages.Lamdatesthomepage;

public class LamdapageTest extends TestBase {

     @Test

    public void lamdatest1() throws InterruptedException {
        start();
        Thread.sleep(5000);
        //scroll code
         javaScriptScrollDown();
         Thread.sleep(5000);
        javaScriptScrollUp(600);


        // click(Lamdatesthomepage.popupclose);
        enterValue(Lamdatesthomepage.lamdatesthomepage1, "Akshata");
        click(Lamdatesthomepage.lamdatestbutton1);
        Util.waitForSec(2);
        Assert.assertEquals(getDriver().findElement(Lamdatesthomepage.Textboxvalue).getText(), "Akshata");

    }

    //using javascript executor
    // @Test
    public void lamdatestJS() {
        start();
        // click(Lamdatesthomepage.popupclose);
        enterValue(Lamdatesthomepage.lamdatesthomepage1, "Akshata");
        javaScriptClick(Lamdatesthomepage.lamdatestbutton1);
        Util.waitForSec(5);
        Assert.assertEquals(getDriver().findElement(Lamdatesthomepage.Textboxvalue).getText(), "Akshata");
    }

   // @Test
    public void lamdatestJSActionClass() {
        start();

       // enterValue(Lamdatesthomepage.lamdatesthomepage1, "Akshata");
        Actions action = new Actions(getDriver());
        WebElement wb1=getDriver().findElement(Lamdatesthomepage.lamdatesthomepage1);
        action.click(wb1).perform();
       // action.sendKeys("Akshata");
       // action.keyDown("A").keyDown("k").keyDown("s").keyDown("h").keyDown("a").keyDown("a").build().perform();

        WebElement wb = getDriver().findElement(Lamdatesthomepage.lamdatestbutton1);
        action.moveToElement(wb).click().build().perform();
        action.doubleClick(wb).perform();
        Util.waitForSec(5);
        Assert.assertEquals(getDriver().findElement(Lamdatesthomepage.Textboxvalue).getText(), "Akshaa");
    }
   // @Test
    public void rightClick()
    {
        start();
        mouseRightClick(Lamdatesthomepage.rightClickButton);

    }
    @Test
    public void doubleClick()
    {
        start();
        mouseDoubleClick(Lamdatesthomepage.doubleClick);
    }

    //@Test
    public void lamdatestJSType() {
        start();
        javaScriptTypeByID("user-message", "Akshata");
        javaScriptClick(Lamdatesthomepage.lamdatestbutton1);
        Util.waitForSec(5);
        Assert.assertEquals(getDriver().findElement(Lamdatesthomepage.Textboxvalue).getText(), "Akshata");
    }

    // @Test
    public void lamdatest2() {
        try {


            start();
            int a = 10, b = 10, c;
           /* String v = "10";
            System.out.println(Integer.parseInt(v));*/
            enterValue(Lamdatesthomepage.Textbox1, String.valueOf(a));
            enterValue(Lamdatesthomepage.Textbox2, String.valueOf(b));
            c = a + b;
            click(Lamdatesthomepage.Button2);
            Util.waitForSec(2);
            Assert.assertEquals(getText(Lamdatesthomepage.Textbox3), String.valueOf(c));
            takeSnapShot("lamdatest2");

        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();

        }

    }

   // @AfterTest
    public void closeBrowser() {
        getDriver().quit();
    }
}
