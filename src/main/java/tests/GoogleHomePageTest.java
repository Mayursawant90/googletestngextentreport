package tests;

import component.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.*;

public class GoogleHomePageTest extends TestBase {

    @Test(groups = {"Regression","Google"})
    public void testRegression_Test_11(){

        log.info("Opening the Url...");
        start();
        enterValue(GoogleHomePage.searchBox,"Mayur"+ Keys.ENTER);
        //click(GoogleHomePage.googleSearchButton);
        Assert.assertTrue(getDriver().getTitle().contains("Mayur"));
    }

    @Test(groups = {"Smoke","Google"})
    public void testRegression_Test_12(){

        log.info("Opening the Url...");
        start();
        enterValue(GoogleHomePage.searchBox,"Mayur"+ Keys.ENTER);
        //click(GoogleHomePage.googleSearchButton);
        Assert.assertTrue(getDriver().getTitle().contains("Mayur"));
    }

    @Test(groups = {"Smoke","Regression","Google"})
    public void testRegression_Test_13(){
        log.info("Opening the Url...");
        start();
        getDriver().findElement(GoogleHomePage.imagesLink).isDisplayed();
        getDriver().findElement(By.xpath( "//a[contains(text(),'Images')]" )).isDisplayed();

        boolean imagesLinkPresent=getDriver().findElement(GoogleHomePage.imagesLink).isDisplayed();
        Assert.assertTrue(imagesLinkPresent);

    }



}
