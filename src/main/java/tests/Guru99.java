package tests;

import component.TestBase;
import component.Util;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.annotations.Test;
import pages.Guru99HomePage;

import java.time.Duration;

public class Guru99 extends TestBase {

    @Test
    public void AlertTestcase() throws InterruptedException {
        start ();
        getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        enterValue(Guru99HomePage.custID,"123");
        Thread.sleep(5000);
        click(Guru99HomePage.submitButton);

       System.out.println(getDriver().switchTo().alert().getText());

        // click ok on popup message

        getDriver().switchTo().alert().accept();

        System.out.println("Ok button clicked succesfully");
        getDriver().switchTo().alert().accept();
    }

}
