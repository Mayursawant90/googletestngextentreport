package tests;

import component.Util;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.FacebookLoginPage;
import component.TestBase;

import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


public class FacebookLoginPageTest extends TestBase {

    @Test(groups = {"Smoke"})
    public void TestFaceBookLogin_Invalid_User_Scenario() {
        try {
            start();
            System.out.println(System.currentTimeMillis());
            //implicit wait
           // getDriver().manage().timeouts().implicitlyWait(Duration.ofSeconds(6));

            //explicit wait
           // WebDriverWait wait=new WebDriverWait(getDriver(), Duration.ofSeconds(5));
            WebElement wb=getDriver().findElement(FacebookLoginPage.facebookUserName);
           // wait.until(ExpectedConditions.visibilityOf(wb));
            //fluent wait
            Wait<WebDriver> wait1 = new FluentWait<WebDriver>(getDriver())
                    .withTimeout(Duration.ofSeconds(30))
                    .pollingEvery(Duration.ofSeconds(5))
                    .ignoring(NoSuchElementException.class);
           WebElement clickseleniumlink = wait1.until(new Function<WebDriver, WebElement>(){
               public WebElement apply(WebDriver driver ) {
                    return wb;
                }
            });

           WebElement wb3=wait1.until(new Function<WebDriver, WebElement>() {
               @Override
               public WebElement apply(WebDriver driver) {
                   return wb;
               }
           });



            clickseleniumlink.sendKeys("Mayur");

            enterValue(FacebookLoginPage.facebookUserName, "mayur");
            System.out.println(System.currentTimeMillis());
            enterValue(FacebookLoginPage.facebookPassword, "mayur");
            click(FacebookLoginPage.facebookLoginButton);
            Util.waitForSec(5);
            Assert.assertTrue(getText(FacebookLoginPage.LinkText).contains("The password that you've entered is incorrect."));
        } finally {
            System.out.println(System.currentTimeMillis());
        }
    }
}
