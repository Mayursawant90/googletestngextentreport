package pages;

import org.openqa.selenium.By;

public class Lamdatesthomepage {
    public static final By lamdatesthomepage1=By.id("user-message");
    public static final By lamdatestbutton1=By.xpath("//button[@id='showInput']");
    public static final By Textboxvalue=By.id("message");
    public static final By popupclose=By.xpath("//span[@id='exit_popup_close']");

    public static final By Textbox1=By.id("sum1");

    public static final By Textbox2=By.id("sum2");
    public static final By Button2=By.xpath("//button[text()='Get Sum']");
    public static final By Textbox3=By.id("addmessage");

    //Guru99 xpath

    public  static final By rightClickButton=By.xpath("//span[text()='right click me']");
    public static final By doubleClick=By.xpath("//button[text()='Double-Click Me To See Alert']");

}
