package pages;

import org.openqa.selenium.By;

public class Guru99HomePage {
    public  static final By submitButton= By.xpath("//input[@type='submit']");
    public  static final By resetButton= By.xpath("//input[@type='reset']");
    public  static final By custID= By.xpath("//input[@name='cusid']");
}
