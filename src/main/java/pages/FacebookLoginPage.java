package pages;

import org.openqa.selenium.By;

public class FacebookLoginPage {
    public static final By facebookUserName=By.id("email");
    public static final By facebookPassword=By.name("pass");
    public static final By facebookLoginButton=By.id("loginbutton");
    public  static  final  By LinkText=By.xpath("//div[@class='_9ay7']");

}
