package pages;

import org.openqa.selenium.By;

public class GoogleHomePage {
    public static final By googleLogo = By.xpath( "//img[@alt='Google']" );
    public static final By searchBox = By.xpath( "//textarea[@title='Search']" );
    public static final By googleSearchButton = By.xpath( "//div[@class='FPdoLc VlcLAe']//input[@name='btnK']" );
    public static final By iAmFeelingLucky = By.xpath( "//div[@class='FPdoLc VlcLAe']//input[@name='btnK']" );
    public static final By imagesLink = By.xpath( "//a[contains(text(),'Images')]" );

}
