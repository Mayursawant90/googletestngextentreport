@objects
    googleLogo              xpath       //img[@alt='Google']
    searchBox               xpath       //input[@name='q']
    googleSearchButton      xpath       //div[@class='FPdoLc VlcLAe']//input[@name='btnK']
    iAmFeelingLucky         xpath       //div[@class='FPdoLc VlcLAe']//input[@name='btnK']


= Main section =
	googleLogo:
		text is "Google"
		css font-size is "42px"
		height 92px
		width 272px

	searchBox:
		height 61px
		width 174px

	googleSearchButton:
		height 61px
        width 174px

	iAmFeelingLucky:
		text is "I'm Feeling Lucky"
		css font-size is "42px"
		height 92px
		width 272px